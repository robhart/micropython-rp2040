import machine
import time

ledpin = machine.Pin(6, machine.Pin.OUT)

while True:
    ledpin.toggle()
    time.sleep(0.5)
    
